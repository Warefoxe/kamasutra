const SEND_MESSAGE = "SEND_MESSAGE";

let initialState = {
  dialogs: [
    { id: 1, name: "Andriy" },
    { id: 2, name: "Yura" },
    { id: 3, name: "Vova" },
    { id: 4, name: "Lesha" },
    { id: 5, name: "Vasya" },
    { id: 6, name: "Dima" },
    { id: 7, name: "Bodya" },
    { id: 8, name: "Sergiy" },
  ],
  messages: [
    { id: 1, message: "Hello" },
    { id: 2, message: "Hi" },
    { id: 3, message: "How are you?" },
  ],
};

const dialogsReducer = (state = initialState, action) => {
  //App -> Dialogs
  switch (action.type) {
    case SEND_MESSAGE:
      let body = action.newMessageBody;
      return {
        ...state,
        messages: [
          ...state.messages,
          {
            id: 4,
            message: body,
          },
        ],
      };

    default:
      return state;
  }
};

//App -> Dialogs
export const sendMessageActionCreate = (newMessageBody) => ({
  type: SEND_MESSAGE,
  newMessageBody,
});

export default dialogsReducer;
