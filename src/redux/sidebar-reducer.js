const SET_FRIENDS = "SET_FRIENDS";

let initialState = {
  friends: [
    { id: 1, name: "Andrew" },
    { id: 2, name: "Sasha" },
    { id: 3, name: "Sveta" },
  ],
};

const sidebarReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_FRIENDS:
      return {
        ...state,
        friends: action.friends,
      };
    default:
      return state;
  }
};

export const setFriend = (friends) => {
  return {
    type: SET_FRIENDS,
    friends,
  };
};

export default sidebarReducer;
