import { profileAPI } from "../app/api/api";

const ADD_POST = "ADD-POST";
const SET_USER_PROFILE = "SET_USER_PROFILE";
const SET_STATUS = "SET_STATUS";

let initialState = {
  posts: [
    { id: 1, message: "Hi, how are you?", like: "105" },
    { id: 2, message: "It's my first post", like: "13" },
  ],
  profile: null,
  status: "",
};

const profileReducer = (state = initialState, action) => {
  //App -> Profile -> MyPosts
  switch (action.type) {
    case ADD_POST: {
      let newPost = {
        id: 3,
        message: action.newPostText,
        like: 15,
      };
      return {
        ...state,
        posts: [...state.posts, newPost],
      };
    }
    case SET_USER_PROFILE: {
      return {
        ...state,
        profile: action.profile,
      };
    }
    case SET_STATUS: {
      return {
        ...state,
        status: action.status,
      };
    }
    default:
      return state;
  }
};

//App -> Profile -> MyPosts
export const addPostActionCreator = (newPostText) => {
  return {
    type: ADD_POST,
    newPostText,
  };
};

export const setUserProfile = (profile) => {
  return {
    type: SET_USER_PROFILE,
    profile,
  };
};

export const setStatus = (status) => {
  return {
    type: SET_STATUS,
    status,
  };
};

export const getUserProfile = (id) => async (dispatch) => {
  let response = await profileAPI.getProfile(id);
  dispatch(setUserProfile(response.data));
};

export const getStatus = (id) => async (dispatch) => {
  let response = await profileAPI.getStatus(id);
  dispatch(setStatus(response.data));
};

export const updateStatus = (status) => (dispatch) => {
  profileAPI.updateStatus(status).then((response) => {
    if (response.data.resultCode === 0) {
      dispatch(setStatus(status));
    }
  });
};

export default profileReducer;
