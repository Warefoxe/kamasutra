import profileReducer from "./profile-reducer";
import dialogsReducer from "./dialogs-reducer";
import sidebarReducer from "./sidebar-reducer";

//store (state, методи, dispatch)

let store = {
  //state
  _state: {
    dialogsPage: {
      dialogs: [
        { id: 1, name: "Andriy" },
        { id: 2, name: "Yura" },
        { id: 3, name: "Vova" },
        { id: 4, name: "Lesha" },
        { id: 5, name: "Vasya" },
        { id: 6, name: "Dima" },
        { id: 7, name: "Bodya" },
        { id: 8, name: "Sergiy" },
      ],
      messages: [
        { id: 1, message: "Hello" },
        { id: 2, message: "Hi" },
        { id: 3, message: "How are you?" },
      ],
      newMessageBody: "hello new message",
    },
    profilePage: {
      posts: [
        { id: 1, message: "Hi, how are you?", like: "105" },
        { id: 2, message: "It's my first post", like: "13" },
      ],
      newPostText: "hello new post",
    },
    sidebar: {
      friends: [
        { id: 1, name: "Andrew" },
        { id: 2, name: "Sasha" },
        { id: 3, name: "Sveta" },
      ],
    },
  },

  _rerenderTree() {
    console.log("State update");
  },
  //getter
  getState() {
    return this._state;
  },
  //observer (pattern) Наблюдатель. Перерисовка index.js
  subscribe(observer) {
    this._rerenderTree = observer;
  },
  //Действие
  dispatch(action) {
    this._state.profilePage = profileReducer(this._state.profilePage, action);
    this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action);
    this._state.sidebar = sidebarReducer(this._state.sidebar, action);

    this._rerenderTree(this._state);
  },
};


export default store;
window.store = store;
