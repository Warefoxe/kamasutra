import React from "react";
import s from "./Dialogs.module.css";
import { Dialog } from "./Dialog/Dialog";
import { Message } from "./Message/Message";
import { reduxForm, Field } from "redux-form";
import { Textarea } from "../../app/layout/FormControls/FormControls";
import { required, maxLengthСreator } from "../../utils/validators/validators";

const maxLength30 = maxLengthСreator(30);

export const Dialogs = (props) => {
  let state = props.dialogsPage;

  let dialogsElements = state.dialogs.map((d) => (
    <Dialog key={d.id} name={d.name} id={d.id} />
  ));

  let messagesElements = state.messages.map((m) => (
    <Message key={m.id} message={m.message} />
  ));

  let addNewMessage = (value) => {
    props.sendMessage(value.newMessageBody);
  };

  return (
    <div className={s.dialogs}>
      <div className={s.dialogsItems}>{dialogsElements}</div>
      <div className={s.messagesItems}>
        <div>{messagesElements}</div>
        <DialogsReduxForm onSubmit={addNewMessage} />
      </div>
    </div>
  );
};

const AddMessageForm = (props) => {
  return (
    <form onSubmit={props.handleSubmit}>
      <div>
        <Field
          component={Textarea}
          validate={[required, maxLength30]}
          name="newMessageBody"
          cols="22"
          placeholder="Enter your message"
        ></Field>
      </div>
      <div>
        <button className={s.btnMessage}>Відправити</button>
      </div>
    </form>
  );
};

const DialogsReduxForm = reduxForm({
  form: "dialogAddMessageForm",
})(AddMessageForm);

export default DialogsReduxForm;
