import React from "react";
import s from "./../Dialogs.module.css";
import { NavLink } from "react-router-dom";

export const Dialog = (props) => {
  return (
    <div className={s.dialog + " " + s.active}>
      <NavLink to={"messages/" + props.id}>{props.name}</NavLink>
    </div>
  );
};