import React from "react";
import Pagination from "../../app/layout/Pagination/Pagination";
import User from "./User";

const Users = (props) => {
  return (
    <div>
      <Pagination
        currentPage={props.currentPage}
        onPageChanged={props.onPageChanged}
        totalUsersCount={props.totalUsersCount}
        pageSize={props.pageSize}
      />

      {props.users.map((u) => (
        <User
          user={u}
          followingInProgress={props.followingInProgress}
          unfollow={props.unfollow}
          follow={props.follow}
          key={u.id}
        />
      ))}
    </div>
  );
};

export default Users;
