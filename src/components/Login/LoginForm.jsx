import React from "react";
import { reduxForm } from "redux-form";
import { createField, Input } from "../../app/layout/FormControls/FormControls";
import { required } from "../../utils/validators/validators";
import s from "../../app/layout/FormControls/FormControls.module.css";

const LoginForm = (props) => {
  return (
    <form onSubmit={props.handleSubmit}>
      {createField("Логін", "email", [required], Input)}
      {/* <Field
          validate={[required]}
          placeholder="Логін"
          name="email"
          component={Input}
        /> */}
      {createField("Пароль", "password", [required], Input, {
        type: "password",
      })}
      {/* <Field
          validate={[required]}
          placeholder="Пароль"
          name="password"
          type="password"
          component={Input}
        /> */}
      {createField(
        null,
        "rememberMe",
        null,
        Input,
        {
          type: "checkbox",
        },
        `Запам'ятати мене`
      )}
      {/* <Field type="checkbox" name="rememberMe" component={Input} />{" "} */}
      {props.error && <div className={s.form_summary_error}>{props.error}</div>}
      <button>Ввійти</button>
    </form>
  );
};

const LoginReduxForm = reduxForm({
  form: "login",
})(LoginForm);

export default LoginReduxForm;
