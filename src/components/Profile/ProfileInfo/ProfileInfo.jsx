import React from "react";
import s from "./ProfileInfo.module.css";
import Loader from "../../../app/layout/Loader/Loader";
import ProfileStatusWithHooks from "./ProfileStatusWithHooks";

export const ProfileInfo = (props) => {
  if (!props.profile) {
    return <Loader />;
  }

  return (
    <div>
      {/* <div>
        <img src="/assets/photo.jpg" alt="photo2"></img>
      </div> */}
      <div className={s.descriptionBlock}>
        <img src={props.profile.photos.large} alt="photolarge" />
        <ProfileStatusWithHooks
          status={props.status}
          updateStatus={props.updateStatus}
        />
      </div>
    </div>
  );
};
