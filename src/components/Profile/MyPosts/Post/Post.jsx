import React from 'react'
import s from './Post.module.css'

export const Post = (props) => {
    return (
      <div className={s.item}>
        <img src="/assets/image4.png" alt="image4"></img>{props.message}
        <div>
          <span>{props.like} like</span>
        </div>
      </div>
    );
}
