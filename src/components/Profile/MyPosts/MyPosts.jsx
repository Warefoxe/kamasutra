import React from "react";
import s from "./MyPosts.module.css";
import { Post } from "./Post/Post";
import { Field, reduxForm } from "redux-form";
import {
  required,
  maxLengthСreator,
} from "../../../utils/validators/validators";
import { Textarea } from "../../../app/layout/FormControls/FormControls";

const maxLength10 = maxLengthСreator(10);

export const MyPosts = (props) => {
  let postsElements = props.posts.map((p) => (
    <Post key={p.id} message={p.message} like={p.like} />
  ));

  let onAddPost = (value) => {
    props.addPost(value.newPostText);
  };

  return (
    <div className={s.postsBlock}>
      <h4>My Posts</h4>
      <PostReduxForm onSubmit={onAddPost} />
      <div className={s.posts}>{postsElements}</div>
    </div>
  );
};

const AddPostForm = (props) => {
  return (
    <form onSubmit={props.handleSubmit}>
      <div className={s.textareaBlock}>
        <Field
          validate={[required, maxLength10]}
          component={Textarea}
          name="newPostText"
          cols="22"
          placeholder="Enter your post"
        />
      </div>
      <div>
        <button className={s.btnAdd}>Add Post</button>
        <button className={s.btnRemove}>Remove</button>
      </div>
    </form>
  );
};

const PostReduxForm = reduxForm({
  form: "postAddPostForm",
})(AddPostForm);
