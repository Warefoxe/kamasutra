import React, { Component } from "react";
import { Navbar } from "./Navbar";
import { connect } from "react-redux";

class NavbarContainer extends Component {
  render() {
    return <Navbar {...this.props} />;
  }

  // let sidebarElements = props.state.friends.map((m) => (
  //   <div className={s.round}> {m.name} </div>
  // ));
}

const mapStateToProps = (state) => {
  return {
    friends: state.sidebar.friends,
  };
};

export default connect(mapStateToProps)(NavbarContainer);
