export const required = (value) => {
  if (value) return undefined;
  return "Поле не може бути пустим";
};

export const maxLengthСreator = (maxLength) => (value) => {
  if (value.length > maxLength) return `Не більше ${maxLength} символів`;
  return undefined;
};

export const maxLength30 = (value) => {
  if (value.length > 30) return "Не більше 30 символів";
  return undefined;
};
