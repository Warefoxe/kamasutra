import React, { Component } from "react";
import "./App.css";
import { News } from "../../components/News/News";
import { Music } from "../../components/Music/Music";
import { Settings } from "../../components/Settings/Settings";
import { Route, BrowserRouter, withRouter } from "react-router-dom";
import DialogsContainer from "../../components/Dialogs/DialogsContainer";
import UsersContainer from "../../components/Users/UsersContainer";
import ProfileContainer from "../../components/Profile/ProfileContainer";
import HeaderContainer from "../../components/Header/HeaderContainer";
import NavbarContainer from "../../components/Navbar/NavbarContainer";
import LoginPage from "../../components/Login/LoginPage";
import { initialize } from "../../redux/app-reducer";
import { compose } from "redux";
import { connect } from "react-redux";
import Loader from "./Loader/Loader";

class App extends Component {
  componentDidMount() {
    this.props.initialize();
  }

  render() {
    if (!this.props.initialized) {
      return <Loader />;
    }

    return (
      <BrowserRouter>
        <div className="app-wrapper">
          <HeaderContainer />
          {/* <Navbar state={props.state.sidebar} /> */}
          <NavbarContainer />
          <div className="app-wrapper-content">
            <Route path="/messages" render={() => <DialogsContainer />} />
            <Route
              path="/profile/:userId?"
              render={() => <ProfileContainer />}
            />

            <Route path="/users" render={() => <UsersContainer />} />
            <Route path="/login" render={() => <LoginPage />} />

            <Route path="/news" component={News} />
            <Route path="/music" component={Music} />
            <Route path="/settings" component={Settings} />
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = (state) => ({
  initialized: state.app.initialized,
});

export default compose(
  withRouter,
  connect(mapStateToProps, { initialize })
)(App);
