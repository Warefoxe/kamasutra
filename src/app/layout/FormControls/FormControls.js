import React from "react";
import { Field } from "redux-form";
import s from "./FormControls.module.css";

export const Textarea = ({ input, meta, ...props }) => {
  const hasError = meta.touched && meta.error;
  return (
    <div className={s.form_control + " " + (hasError ? s.error : "")}>
      <div>
        <textarea {...input} {...props} />
      </div>
      {hasError && <span>{meta.error}</span>}
    </div>
  );
};

export const Input = ({ input, meta, ...props }) => {
  const hasError = meta.touched && meta.error;
  return (
    <div className={s.form_control + " " + (hasError ? s.error : "")}>
      <div>
        <input {...input} {...props} />
      </div>
      {hasError && <span>{meta.error}</span>}
    </div>
  );
};

export const createField = (
  placeholder,
  name,
  validate,
  component,
  props = {},
  text = ""
) => (
  <div>
    <Field
      validate={validate}
      placeholder={placeholder}
      name={name}
      component={component}
      {...props}
    />{" "}
    {text};
  </div>
);
